
alias ll='ls -lat'
alias ltr='ls -latr'
alias lsd='ls -l | grep ^d'
alias lsl='ll -l | grep ^l'
alias dirtrim_1='export PROMPT_DIRTRIM=1'
alias dirtrim_2='export PROMPT_DIRTRIM=2'
alias dirtrim_3='export PROMPT_DIRTRIM=3'

#local tweaks

# Local overrides go here after creating a machine-specific branch
